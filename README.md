# OSM Localities

Countries, Regions, Districts (Counties) and Localities (City, Town, Village)
with coordinates and boundary polygons, synced from OpenStreetMap database.

This module creates geographic entities and imports information from OSM
database manually via Batch GUI, and keeping them in sync via background Cron
jobs, importing all last changes in synced OSM Elements by changed timestamp.

Module uses Overpass API queries with area search to find all objects inside
Countries and subareas (Regions, Counties, Districts) boundaries - even
localities, that didn't have filled tags with relation to any country or
subarea!

# Requirements

- Drupal 8 or higher.
- [Overpass API](https://www.drupal.org/project/overpass_api) module for
querying data from OpenStreetMap database using Overpass query language.
- [Geofield](https://www.drupal.org/project/geofield) module for store coordinates and boundary geometry.
- [Leaflet](https://www.drupal.org/project/leaflet) module for display bounraries in map in UI.
- PHP library `spatie/data-transfer-object`, should be installed automatically with Composer.
- Optionally [php-geos](https://git.osgeo.org/gitea/geos/php-geos) extension to merge areas, if this is neeede to join several areas via overide rules.

# Configuration

Configuration form is located on `/admin/config/system/osm-localities` url, where you can configure preferred languages of OSM objects to import and other options.
