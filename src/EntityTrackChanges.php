<?php

namespace Drupal\osm_localities;

use Drupal\Component\Utility\DiffArray;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Track changes of entity field values.
 */
class EntityTrackChanges {

  /**
   * Array of tracked fields.
   *
   * @var array
   */
  protected $trackedFields;

  /**
   * Encode type for data.
   *
   * @var array
   */
  protected $encodeType;

  /**
   * Original values of entity fields.
   *
   * @var array
   */
  protected $entityTrackDataOriginal;

  /**
   * Get original values of entity, and list of tracked fields.
   *
   * @param Drupal\Core\Entity\EntityInterface $entity
   *   Entity with original data.
   * @param array|string $trackedFields
   *   Array with list of field id's to track, or string for one field.
   */
  public function __construct(EntityInterface $entity, $trackedFields = NULL) {
    if (is_string($trackedFields)) {
      $trackedFields = [$trackedFields];
    }
    $this->trackedFields = $trackedFields;
    if ($this->trackedFields == NULL) {
      $this->entityTrackDataOriginal = self::generateTrackData($entity);
    }
    else {
      foreach ($this->trackedFields as $trackedField) {
        $this->entityTrackDataOriginal[$trackedField] = self::generateTrackData($entity->{$trackedField});
      }
    }
  }

  /**
   * Check if entity field values changed from original, or not.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity with updated field values to check.
   *
   * @return bool|array
   *   TRUE if changed, FALSE if not changed, or array with changes
   *   (empty if none changed)
   */
  public function isChanged(EntityInterface $entity) {
    if ($this->trackedFields == NULL) {
      $entityValues = self::generateTrackData($entity);
      return !($entityValues == $this->entityTrackDataOriginal);
    }
    else {
      foreach ($this->trackedFields as $trackedField) {
        if (self::generateTrackData($entity->{$trackedField}) != $this->entityTrackDataOriginal[$trackedField]) {
          return TRUE;
        }
      }
      return FALSE;
    }
  }

  /**
   * Check if entity field values differ.
   *
   * @return bool
   *   TRUE if differ, FALSE if not
   */
  public static function isEntityValuesDiffer(EntityInterface $entity1, EntityInterface $entity2, $trackedFields = NULL, bool $returnChanges = FALSE): bool {
    if (is_string($trackedFields)) {
      $trackedFields = [$trackedFields];
    }
    if ($trackedFields == NULL) {
      $entity1Values = self::generateTrackData($entity1);
      $entity2Values = self::generateTrackData($entity2);
    }
    else {
      foreach ($trackedFields as $trackedField) {
        $entity1Values[$trackedField] = self::generateTrackData($entity1->{$trackedField});
        $entity2Values[$trackedField] = self::generateTrackData($entity2->{$trackedField});
      }
    }
    $diffFull = array_merge(DiffArray::diffAssocRecursive($entity2Values, $entity1Values), DiffArray::diffAssocRecursive($entity1Values, $entity2Values));
    return $returnChanges ? $diffFull : (empty($diffFull) ? FALSE : TRUE);
  }

  /**
   * Generates unique hash of field data.
   */
  private static function generateTrackData($variable) {
    if (is_object($variable)) {
      if (is_a($variable, EntityInterface::class)) {
        $variable = $variable->toArray();
      }
      if (is_a($variable, FieldItemListInterface::class)) {
        $variable = $variable->getValue();
      }
    }
    return hash("sha256", serialize($variable));
  }

}
