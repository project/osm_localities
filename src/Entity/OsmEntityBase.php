<?php

namespace Drupal\osm_localities\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\osm_localities\OsmLocalities;
use Drupal\osm_localities\Utils;

/**
 * Base class for OSM Locality entities.
 */
abstract class OsmEntityBase extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $entityTypeId = $entity_type->id();
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDisplayConfigurable('view', TRUE)
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel('Name')
      ->setDescription('Name of the element')
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['coordinates'] = BaseFieldDefinition::create('geofield')
      ->setLabel('Coordinates')
      ->setDescription('GPS coordinates of object.')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'leaflet_formatter_default',
        'settings' => [
          'disable_wheel' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $fields['wikidata_id'] = BaseFieldDefinition::create('string')
      ->setLabel('Wikidata ID of the object')
      ->setDisplayConfigurable('view', TRUE)
      ->setDisplayConfigurable('form', TRUE);

    $entityTypeData = OsmLocalities::getEntityTypeData($entityTypeId);
    if ($entityTypeData->parentEntityType) {
      $fields[$entityTypeData->parentField] = Utils::entityReferenceBaseFieldDefinition($entityTypeData->parentEntityType)
        ->setLabel($entityTypeData->parentFieldLabel)
        ->setDescription('Parent object.')
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);
    }

    if ($entityTypeData->selectiveSync) {
      $fields['is_sync_enabled'] = BaseFieldDefinition::create('boolean')
        ->setLabel('Sync is enabled')
        ->setDescription('Enables sync children entities for this entity.')
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE);
    }

    if ($entityTypeId !== 'osm_locality') {
      $fields['admin_center'] = Utils::entityReferenceBaseFieldDefinition('osm_locality')
        ->setLabel('Administrative center')
        ->setDescription('Link to locality, marked as admin center of this area.');

      $fields['boundary'] = Utils::entityReferenceBaseFieldDefinition('osm_geometry', 'default')
        ->setLabel('Boundary detailed')
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE)
        ->setDescription('Boundary multipolygon with maximum detalization.');

      $fields['boundary_s'] = Utils::entityReferenceBaseFieldDefinition('osm_geometry', 'default')
        ->setLabel('Boundary simplified')
        ->setDisplayConfigurable('view', TRUE)
        ->setDisplayConfigurable('form', TRUE)
        ->setDescription('Simplified boundary region with reduced size, that uses less points.');

      if ($entityTypeId !== 'osm_country') {
        $fields['is_parent_area'] = BaseFieldDefinition::create('boolean')
          ->setLabel('Created as parent area')
          ->setDisplayConfigurable('view', TRUE)
          ->setDisplayConfigurable('form', TRUE)
          ->setDescription('Indicates that this object is created from parent area, because of missing chidlren elements.');
      }
    }
    return $fields;
  }

  /**
   *
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    $geometryEntitiesToDeleteIds = [];
    $osmGeometryStorage = \Drupal::entityTypeManager()->getStorage('osm_geometry');

    foreach ($entities as $entity) {
      if ($entity->hasField('boundary') && !$entity->boundary->isEmpty()) {
        $geometryEntitiesToDeleteIds[] = $entity->boundary->target_id;
      }
      if ($entity->hasField('boundary_s') && !$entity->boundary_s->isEmpty()) {
        $geometryEntitiesToDeleteIds[] = $entity->boundary_s->target_id;
      }
    }
    // We need to load geometry entities one by one to exclude memory problems.
    foreach ($geometryEntitiesToDeleteIds as $id) {
      $geometryEntity = $osmGeometryStorage->load($id);
      $geometryEntity->delete();
      $osmGeometryStorage->resetCache();
    }
    parent::postDelete($storage, $entities);
  }

}
