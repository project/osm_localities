<?php

namespace Drupal\osm_localities\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the OSM Geometry entity.
 *
 * @ingroup brick_helper
 *
 * @ContentEntityType(
 *   id = "osm_geometry",
 *   label = @Translation("OSM Geometry"),
 *   base_table = "osm_geometry",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\Core\Entity\EntityListBuilder",
 *     "local_action_provider" = {
 *       "collection" = "Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   translatable = FALSE,
 *   admin_permission = "administer osm_localities entities",
 *   links = {
 *     "canonical" = "/admin/structure/localities/osm_geometry/{osm_geometry}",
 *     "collection" = "/admin/structure/localities/osm_geometry",
 *   },
 * )
 */
class OsmGeometry extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['geometry'] = BaseFieldDefinition::create('geofield')
      ->setLabel(t('Geometry'))
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'leaflet_formatter_default',
        'settings' => [
          'disable_wheel' => TRUE,
    // leaflet_map: 'OSM Mapnik'.
        ],
      ]);
    return $fields;
  }

}
