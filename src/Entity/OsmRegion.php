<?php

namespace Drupal\osm_localities\Entity;

use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the OSM Region entity.
 *
 * @ingroup osm_localities
 *
 * @ContentEntityType(
 *   id = "osm_region",
 *   label = @Translation("Region"),
 *   base_table = "osm_region",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\osm_localities\OsmEntityListBuilder",
 *     "local_action_provider" = {
 *       "collection" = "Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   translatable = FALSE,
 *   admin_permission = "administer osm_localities entities",
 *   links = {
 *     "canonical" = "/admin/structure/localities/osm_region/{osm_region}",
 *     "collection" = "/admin/structure/localities/osm_region",
 *     "edit-form" = "/admin/structure/localities/osm_region/{osm_region}/edit",
 *     "delete-form" = "/admin/structure/localities/osm_region/{osm_region}/delete",
 *   },
 *   field_ui_base_route = "entity.osm_region.collection"
 * )
 */
class OsmRegion extends OsmEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id']
      ->setLabel('ID')
      ->setDescription('ID of relation in OSM database')
      ->setSettings([
        'unsigned' => TRUE,
        'size' => 'big',
      ]);

    return $fields;
  }

}
