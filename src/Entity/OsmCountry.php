<?php

namespace Drupal\osm_localities\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the OSM Country entity.
 *
 * @ingroup osm_localities
 *
 * @ContentEntityType(
 *   id = "osm_country",
 *   label = @Translation("Country"),
 *   base_table = "osm_country",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\osm_localities\OsmEntityListBuilder",
 *     "local_action_provider" = {
 *       "collection" = "Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   translatable = FALSE,
 *   admin_permission = "administer osm_localities entities",
 *   links = {
 *     "canonical" = "/admin/structure/localities/osm_country/{osm_country}",
 *     "collection" = "/admin/structure/localities/osm_country",
 *     "edit-form" = "/admin/structure/localities/osm_country/{osm_country}/edit",
 *     "delete-form" = "/admin/structure/localities/osm_country/{osm_country}/delete",
 *   }
 * )
 */
class OsmCountry extends OsmEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id']
      ->setLabel('ID')
      ->setDescription('ID of relation in OSM database.')
      ->setSettings([
        'unsigned' => TRUE,
        'size' => 'big',
      ]);

    $fields['code'] = BaseFieldDefinition::create('string')
      ->setLabel('Code')
      ->setDescription('ISO3166-1:alpha2 code of the country.')
      ->setSettings([
        'max_length' => 2,
      ]);

    return $fields;
  }

}
