<?php

namespace Drupal\osm_localities\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the OSM OsmCounty entity.
 *
 * @ingroup osm_localities
 *
 * @ContentEntityType(
 *   id = "osm_county",
 *   label = @Translation("County"),
 *   base_table = "osm_county",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\osm_localities\OsmEntityListBuilder",
 *     "local_action_provider" = {
 *       "collection" = "Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   translatable = FALSE,
 *   admin_permission = "administer osm_localities entities",
 *   links = {
 *     "canonical" = "/admin/structure/localities/osm_county/{osm_county}",
 *     "collection" = "/admin/structure/localities/osm_county",
 *     "edit-form" = "/admin/structure/localities/osm_county/{osm_county}/edit",
 *     "delete-form" = "/admin/structure/localities/osm_county/{osm_county}/delete",
 *   }
 * )
 */
class OsmCounty extends OsmEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id']
      ->setLabel('ID')
      ->setDescription('ID of relation in OSM database')
      ->setSettings([
        'unsigned' => TRUE,
        'size' => 'big',
      ]);

    $fields['is_parent_area'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Created as parent area')
      ->setDescription('Indicates that this object is created from parent area, because of missing chidlren elements.');

    return $fields;
  }

}
