<?php

namespace Drupal\osm_localities\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\osm_localities\Utils;

/**
 * Defines the OSM Locality entity.
 *
 * @ingroup osm_localities
 *
 * @ContentEntityType(
 *   id = "osm_locality",
 *   label = @Translation("Locality"),
 *   base_table = "osm_locality",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *   },
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "edit" = "Drupal\Core\Entity\ContentEntityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\osm_localities\OsmEntityListBuilder",
 *     "local_action_provider" = {
 *       "collection" = "Drupal\entity\Menu\EntityCollectionLocalActionProvider",
 *     },
 *     "permission_provider" = "Drupal\entity\EntityPermissionProvider",
 *     "route_provider" = {
 *       "default" = "Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider",
 *     },
 *     "views_data" = "Drupal\views\EntityViewsData",
 *   },
 *   translatable = FALSE,
 *   admin_permission = "administer osm_localities entities",
 *   links = {
 *     "canonical" = "/admin/structure/localities/osm_locality/{osm_locality}",
 *     "collection" = "/admin/structure/localities/osm_locality",
 *     "edit-form" = "/admin/structure/localities/osm_locality/{osm_locality}/edit",
 *     "delete-form" = "/admin/structure/localities/osm_locality/{osm_locality}/delete",
 *   },
 *   field_ui_base_route = "entity.osm_locality.collection"
 * )
 */
class OsmLocality extends OsmEntityBase {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Field\BaseFieldDefinition[] $fields */
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id']
      ->setLabel('ID')
      ->setDescription('ID of node in OSM database')
      ->setSettings([
        'unsigned' => TRUE,
        'size' => 'big',
      ]);

    $fields['region'] = Utils::entityReferenceBaseFieldDefinition('osm_region')
      ->setLabel('Region')
      ->setDescription('Related region');

    $fields['country'] = Utils::entityReferenceBaseFieldDefinition('osm_country')
      ->setLabel('Country')
      ->setDescription('Related country');

    $fields['address'] = BaseFieldDefinition::create('string')
      ->setLabel('Address')
      ->setDescription('Address string: County, Region, Country')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ]);

    $fields['is_region_center'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Center of the region')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ]);

    $fields['is_county_center'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Center of the county')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ]);

    $fields['is_capital'] = BaseFieldDefinition::create('boolean')
      ->setLabel('Capital of the country')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ]);

    $fields['type'] = BaseFieldDefinition::create('list_string')
      ->setLabel("Locality type")
      ->setSettings([
        'allowed_values' => [
          'city' => 'City',
          'town' => 'Town',
          'village' => 'Village',
        ],
      ])
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ]);

    $fields['population'] = BaseFieldDefinition::create('integer')
      ->setLabel('Population')
      ->setDescription('Population')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
      ]);

    return $fields;
  }

}
