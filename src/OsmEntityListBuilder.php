<?php

namespace Drupal\osm_localities;

use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\osm_localities\DTO\OsmLocalitiesEntityTypeData;

/**
 * Provides a list controller for entity.
 */
class OsmEntityListBuilder extends EntityListBuilder {

  protected ContentEntityType $childEntityType;
  protected OsmLocalitiesEntityTypeData $entityTypeOsmInfo;
  protected OsmLocalitiesEntityTypeData $parentEntityTypeOsmInfo;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage) {
    parent::__construct($entity_type, $storage);
    $this->entityTypeOsmInfo = OsmLocalities::getEntityTypeData($this->entityTypeId);
    if ($this->entityTypeOsmInfo->childEntityType) {
      $this->childEntityType = \Drupal::service('entity_type.manager')->getDefinition($this->entityTypeOsmInfo->childEntityType);
    }
    if ($this->entityTypeOsmInfo->parentEntityType) {
      $this->parentEntityTypeOsmInfo = OsmLocalities::getEntityTypeData($this->entityTypeOsmInfo->parentEntityType);
    }

  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    if ($this->entityTypeOsmInfo->parentField) {
      $header['parent'] = $this->t('Parent element');
    }
    if ($this->entityTypeOsmInfo->selectiveSync ?? NULL) {
      $header['is_sync_enabled'] = $this->t('Sync enabled');
    }
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $route = "entity.{$this->entityTypeId}.canonical";
    $routeArguments = [$this->entityTypeId => $entity->id()];

    $row['name'] = new Link($entity->label() ?? '[' . $entity->id() . ']', Url::fromRoute($route, $routeArguments));

    if ($this->entityTypeOsmInfo->parentField) {
      $parentFieldName = $this->entityTypeOsmInfo->parentField;
      if ($parentFieldName) {
        if ($parentEntity = $entity->{$parentFieldName}->entity) {
          $row['parent'] = $parentEntity->toLink();
        }
        else {
          $row['parent'] = 'Broken link';
        }
      }
      else {
        $row['parent'] = '-';
      }
    }
    if ($this->entityTypeOsmInfo->selectiveSync ?? NULL) {
      $row['is_sync_enabled'] = $entity->is_sync_enabled->value ? '✔️' : '-';
    }

    return array_merge($row, parent::buildRow($entity));
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $entityQuery = $this->getStorage()->getQuery();

    if ($this->entityTypeOsmInfo->parentEntityType) {
      $parentEntityQuery = \Drupal::EntityQuery($this->entityTypeOsmInfo->parentEntityType);
      if ($this->parentEntityTypeOsmInfo->selectiveSync) {
        $parentEntityQuery->condition('is_sync_enabled', TRUE);
      }
      $syncEnabledCount = $parentEntityQuery->count()->execute();
      $syncEnabled = ($syncEnabledCount > 0);
    }
    else {
      $syncEnabled = TRUE;
      $syncEnabledCount = NULL;
    }

    $totalCount = $entityQuery->count()->execute();

    $headerMessages = [];

    if ($this->childEntityType ?? NULL) {
      $linkToChildren = new Link(
        $this->t('Go to :entityTypeLabel', [':entityTypeLabel' => $this->childEntityType->getLabel()]),
        Url::fromRoute('entity.' . $this->entityTypeOsmInfo->childEntityType . '.collection', [],
        [
          'attributes' => ['class' => ['button', 'button--secondary', 'button--small']],
        ])
      );
      $headerMessages['linkToChildren'] = $linkToChildren->toString();
    }

    $lastSyncTimestampUpdate = OsmLocalities::getEntityTypeSyncTimestamp($this->entityTypeId, 'update');
    $lastSyncTimestampResync = OsmLocalities::getEntityTypeSyncTimestamp($this->entityTypeId, 'resync');

    $headerMessages['syncTimes'] = $this->t('Last sync: :lastSyncTime, full resync: :lastFullSyncTime.', [
      ':lastSyncTime' => $lastSyncTimestampUpdate ? \Drupal::service('date.formatter')->format($lastSyncTimestampUpdate) : $this->t('never'),
      ':lastFullSyncTime' => $lastSyncTimestampResync ? \Drupal::service('date.formatter')->format($lastSyncTimestampResync) : $this->t('never'),
    ]);

    if (!$syncEnabled) {
      \Drupal::messenger()->addWarning($this->t('Sync is disabled because of syncable <a href="@parentUrl">@parentTypeLabel</a> items are missing.', [
        '@parentTypeLabel' => $this->entityTypeOsmInfo->parentFieldLabel,
        '@parentUrl' => Url::fromRoute('entity.' . $this->entityTypeOsmInfo->parentEntityType . '.collection')->toString(),
      ]));
    }
    else {
      if ($syncEnabledCount > 0) {
        $headerMessages['syncCount'] = $this->t('Sync enabled for :count parent items.', [':count' => $syncEnabledCount]);
      }
      if ($lastSyncTimestampResync) {
        $updateLink = new Link(
        $this->t('Sync recent changes'),
        Url::fromRoute('osm_localities.sync_batch', ['entityType' => $this->entityTypeId], [
          'attributes' => ['class' => ['button', 'button--primary', 'button--small']],
        ]));
        $headerMessages['updateLink'] = $updateLink->toString();
      }
      $resyncLink = new Link(
        $this->t('Full resync'),
        Url::fromRoute('osm_localities.sync_batch', ['entityType' => $this->entityTypeId], [
          'query' => ['mode' => 'resync'],
          'attributes' => ['class' => ['button', 'button--secondary', 'button--small']],
        ]));
      $headerMessages['resyncLink'] = $resyncLink->toString();
    }

    $build['header'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => implode(' ', $headerMessages),
    ];
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\osm_localities\Form\EntityListBuilderFilterForm', $this->entityTypeId);

    $build += parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = \Drupal::entityQuery($this->entityTypeId);
    $request = \Drupal::request();
    $entityTypeData = OsmLocalities::getEntityTypeData($this->entityTypeId);

    if (
      $entityTypeData->parentField
      && $parentId = $request->get($entityTypeData->parentField)
    ) {
      $query->condition($entityTypeData->parentField, $parentId);
    }
    if ($value = $request->get('name') ?? NULL) {
      $query->condition('name', '%' . $value . '%', 'LIKE');
    }
    if ($value = $request->get('is_county_center') ?? NULL) {
      $query->condition('is_county_center', $value);
    }
    if ($value = $request->get('is_region_center') ?? NULL) {
      $query->condition('is_region_center', $value);
    }
    if ($value = $request->get('is_capital') ?? NULL) {
      $query->condition('is_capital', $value);
    }

    $query->sort('name');

    if ($this->limit) {
      $query->pager($this->limit);
    }

    return $query->execute();
  }

}
