<?php

namespace Drupal\osm_localities;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\osm_localities\DTO\OsmLocalitiesEntityTypeData;
use Drupal\osm_localities\DTO\OsmLocalitiesSyncOptions;
use Drupal\osm_localities\DTO\OsmLocalitiesSyncResult;
use Drupal\overpass_api\DTO\OverpassQueryOptions;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Yaml\Yaml;

/**
 * Helper functions for OSM Localities module.
 */
class OsmLocalities {

  /**
   * Returns state key to use for operations.
   */
  public static function getStateKeyForEntityType(string $operation, string $entityType, string $operationType) {
    return "osm_localities.$operation.$entityType.$operationType";
  }

  /**
   * Returns last sync timestamp for entity type.
   *
   * @param string $entityType
   *   Id of entity type.
   * @param update|resync $entityType
   *   Sync mode: update | resync.
   *
   * @return int|array[int]
   */
  public static function getEntityTypeSyncTimestamp(string $entityType, string $mode = 'max') {
    if ($mode == "update" || $mode == 'resync') {
      return \Drupal::state()->get(OsmLocalities::getStateKeyForEntityType('sync', $entityType, 'last_' . $mode . '_time'));
    }
    $timestamps['update'] = \Drupal::state()->get(OsmLocalities::getStateKeyForEntityType('sync', $entityType, 'last_update_time'));
    $timestamps['resync'] = \Drupal::state()->get(OsmLocalities::getStateKeyForEntityType('sync', $entityType, 'last_resync_time'));
    if ($mode == "max") {
      return max($timestamps['update'], $timestamps['resync']);
    }
    throw new \Exception('Unknown mode: ' . $mode);
  }

  /**
   * Sets last sync timestamp for entity type.
   *
   * @param string $entityType
   *   Id of entity type.
   * @param update|resync $entityType
   *   Sync mode: update | resync.
   * @param int $timestamp
   *   Timestamp to set.
   */
  public static function setEntityTypeSyncTimestamp(string $entityType, string $mode = NULL, int $timestamp) {
    \Drupal::state()->set(OsmLocalities::getStateKeyForEntityType('sync', $entityType, 'last_' . $mode . '_time'), $timestamp);
  }

  /**
   * Finds optimal tag version by selected language priority.
   */
  public static function getPrefferedNameFromTags(string $tagName, array $tags, bool $returnNotSetText = TRUE) {
    static $languages;
    if (!$languages) {
      $languagesSetting = \Drupal::config('osm_localities.settings')->get('languages') ?? 'default';
      if (empty($languagesSetting)) {
        $languagesSetting = \Drupal::languageManager()->getCurrentLanguage()->getId() . ',en,default';
      }
      $languages = explode(',', $languagesSetting);
    }

    foreach ($languages as $language) {
      $tagWithLanguage =
        $tagName
        . ($language == 'default' ? '' : (':' . $language));
      if (isset($tags[$tagWithLanguage])) {
        return $tags[$tagWithLanguage];
      }
    }
    return $returnNotSetText
        ? \Drupal::service('string_translation')->translate('Not set')
        : NULL;
  }

  /**
   * Generates overpass query by type of object.
   *
   * @param \Drupal\osm_localities\DTO\OsmLocalitiesSyncOptions $syncOptions
   *   Options object.
   */
  public static function generateOverpassQuery(OsmLocalitiesSyncOptions $syncOptions) {
    $queryParams = [];
    $outType = 'geom';
    $filters = [];
    if ($syncOptions->filter) {
      $filters[] = $syncOptions->filter;
    }
    if ($syncOptions->entityType == 'osm_locality') {
      $outType = 'body';
      static $placeRegex;
      if (!$placeRegex) {
        $localityTypes = \Drupal::config('osm_localities.settings')->get('locality_types') ?? ['city', 'town'];
        $placeRegex = '^(' . implode('|', $localityTypes) . ')$';
      }

      $queryObject = 'node';
      $queryParams[] = "['place'~'$placeRegex']";
    }
    else {
      if ($syncOptions->entityType == 'osm_country') {
        $adminLevel = 2;
        $filters[] = '["ISO3166-1:alpha2"~"^..$"]';
      }
      elseif ($syncOptions->entityType == 'osm_region') {
        $adminLevel = 4;
      }
      elseif ($syncOptions->entityType == 'osm_county') {
        $adminLevel = 6;
      }
      else {
        throw new \Exception("Unknown entity type $syncOptions->entityType");
      }
      $queryObject = 'relation';
      $queryParams[] = "['admin_level'='$adminLevel']\n['boundary'='administrative']";
    }
    if ($filters) {
      $queryParams['filter'] = implode("\n", $filters);
    }
    if (($syncOptions->fromId ?? NULL) > 1) {
      $queryParams['fromId'] = "(if:id() >= {$syncOptions->fromId})";
    }

    $areas = [];
    $queries = [];
    $filterAreasIds = [];
    if ($syncOptions->parentId) {
      $entityTypeData = self::getEntityTypeData($syncOptions->entityType);
      $areaName = 'parentArea';
      $filterAreasIds[$areaName] = "relation({$syncOptions->parentId}); map_to_area->.$areaName;";
      $overrideRules = self::getOverrideRules($entityTypeData->parentEntityType);
      if (isset($overrideRules[$syncOptions->parentId]['joinAreas'])) {
        foreach ($overrideRules[$syncOptions->parentId]['joinAreas'] as $delta => $value) {
          $areaName = 'parentAreaAttached' . $delta;
          $filterAreasIds[$areaName] =
            "relation($value); map_to_area->.$areaName;";
        }
      }
      foreach ($overrideRules[$syncOptions->parentId]['children']['ignore'] ?? [] as $id) {
        $queryParams[] = "(if:id() != $id)";
      }
      foreach ($overrideRules[$syncOptions->parentId]['children']['add'] ?? [] as $id) {
        $query = "$queryObject($id)";
        if (isset($queryParams['fromId'])) {
          $query .= "\n  {$queryParams['fromId']}";
        }
        $queries[] = $query;
      }
    }

    $queryParamsString = self::indent(implode(PHP_EOL, $queryParams), 2);

    if (count($filterAreasIds) == 0) {
      $queries[] = "$queryObject\n$queryParamsString";
    }
    else {
      foreach ($filterAreasIds as $filterAreasId => $filterAreaString) {
        $areas[] = $filterAreaString;
        $queries[] = "$queryObject(area.$filterAreasId)\n$queryParamsString";
      }
    }
    $query = implode("\n", $areas) . "\n(\n" . implode(";\n", $queries) . ";\n)\n";

    return [
      'query' => $query,
      'out' => $outType,
    ];
  }

  /**
   * Returns entity type data for processing sync requests.
   *
   * @param string $entityType
   *   Type of entity.
   * @param parent|overpass $dataType
   *   Type of data to return.
   * @param array{fromId: int, parentId: int}|null $options
   *   id of parent entity, only for dataType=overpassQuery.
   *
   * @return \Drupal\osm_localities\DTO\OsmLocalitiesEntityTypeData
   *   Object with data.
   */
  public static function getEntityTypeData(string $entityType) {

    // osm_country entity type.
    if ($entityType == 'osm_country') {
      return new OsmLocalitiesEntityTypeData([
        'parentField' => NULL,
        'childEntityType' => 'osm_region',
        'selectiveSync' => TRUE,
      ]);
    }

    // osm_region entity type.
    if ($entityType == 'osm_region') {
      return new OsmLocalitiesEntityTypeData([
        'parentField' => 'country',
        'parentFieldLabel' => 'Country',
        'parentEntityType' => 'osm_country',
        'childEntityType' => 'osm_county',
        'emptyChildrenUseParent' => TRUE,
        'selectiveSync' => FALSE,
      ]);
    }

    // osm_county entity type.
    if ($entityType == 'osm_county') {
      return new OsmLocalitiesEntityTypeData([
        'parentField' => 'region',
        'parentFieldLabel' => 'Region',
        'parentEntityType' => 'osm_region',
        'childEntityType' => 'osm_locality',
        'emptyChildrenUseParent' => TRUE,
        'selectiveSync' => FALSE,
      ]);
    }

    // osm_locality entity type.
    if ($entityType == 'osm_locality') {
      return new OsmLocalitiesEntityTypeData([
        'parentEntityType' => 'osm_county',
        'parentField' => 'county',
        'parentFieldLabel' => 'County',
        'selectiveSync' => FALSE,
      ]);
    }
    throw new \Exception("Unknown entityType $entityType");
  }

  /**
   *
   */
  public static function osmEntityTypeRemoteCount(OsmLocalitiesSyncOptions $syncOptions) {
    $nameConverter = new CamelCaseToSnakeCaseNameConverter(NULL, FALSE);

    $entityClassName = '\\Drupal\\osm_localities\\Entity\\' . $nameConverter->denormalize($syncOptions->entityType);
    $overpassRequest = self::generateOverpassQuery($syncOptions);

    $overpassQueryElements = \Drupal::service('overpass_api')->query(
    $overpassRequest['query'], new OverpassQueryOptions([
      'out' => 'count',
    ]));
    $count = $overpassQueryElements[0]->tags->total;
    return $count;
  }

  /**
   * Performs a sync of entity type with OSM database by chunk.
   *
   * @param \Drupal\osm_localities\DTO\OsmLocalitiesSyncOptions $syncOptions
   *   Id of entity type.
   *
   * @return array{processed: int, created: int, updated: int, deleted: int, idLast: int}
   *   Operation statistic and last id of chunk.
   */
  public static function osmEntityTypeSyncChunk(OsmLocalitiesSyncOptions $syncOptions) {
    $microtimeStart = \Drupal::time()->getCurrentMicroTime();
    // Trying to increase limits and force clean the memory, because operation
    // is very memory and time hungry.
    Utils::extendLimits();
    Utils::forceMemClean();

    $result = new OsmLocalitiesSyncResult();

    $entityStorage = \Drupal::entityTypeManager()->getStorage($syncOptions->entityType);
    $entityGeometryStorage = \Drupal::entityTypeManager()->getStorage('osm_geometry');
    $entityTypeData = self::getEntityTypeData($syncOptions->entityType);

    if ($entityTypeData->parentField && empty($syncOptions->parentId)) {
      throw new \Exception("Parent id is reqired to sync $syncOptions->entityType type");
    }

    $overpassRequest = self::generateOverpassQuery($syncOptions);

    $overrideRules = self::getOverrideRules($syncOptions->entityType);

    $overpassQueryResult = \Drupal::service('overpass_api')->query(
      $overpassRequest['query'],
      new OverpassQueryOptions([
        'out' => $overpassRequest['out'],
        'limit' => $syncOptions->limit,
        'returnRaw' => TRUE,
      ])
    );
    $fileSystemService = \Drupal::service('file_system');
    $tmpFilePath = $fileSystemService->realpath(
    $fileSystemService->tempnam('temporary://', 'overpass_api_response_')
    );
    file_put_contents($tmpFilePath, $overpassQueryResult);
    unset($overpassQueryResult);
    Utils::forceMemClean();

    $elementsRemoteReader = new OverpassReplyReader($tmpFilePath);

    $elementsProcessedIds = [];

    for ($i = 0; $i < $syncOptions->limit; $i++) {
      $elementRemote = $elementsRemoteReader->getElement();
      if ($elementRemote === NULL) {
        break;
      }
      $id = $elementRemote['id'];
      Utils::forceMemClean();
      if (isset($overrideRules[$id])) {
        /**
         * list of additional areas to join.
         *.
         * @var array<int>
         * key - areaId to join, value - to what element join it.
         */
        $areasAdditional = [];
        foreach ($overrideRules[$id]['joinAreas'] ?? [] as $joinAreaId) {
          $areasAdditional[$joinAreaId] = $id;
        }
        if (!empty($areasAdditional)) {
          foreach ($areasAdditional as $areaAdditionalId => $targetId) {
            $areasAdditionalQueries[] = "relation($areaAdditionalId);";
          }
          $overpassQueryAdditionalElementsRaw = \Drupal::service('overpass_api')->query(
            '(' . implode("\n", $areasAdditionalQueries) . ')',
            new OverpassQueryOptions([
              'out' => $overpassRequest['out'],
              'returnRaw' => TRUE,
            ])
          );
          $overpassQueryAdditionalElementsObject = json_decode($overpassQueryAdditionalElementsRaw, JSON_OBJECT_AS_ARRAY);
          $overpassQueryAdditionalElementsRaw = NULL;
          $overpassQueryAdditionalElements = $overpassQueryAdditionalElementsObject['elements'];
          $overpassQueryAdditionalElementsObject = NULL;
          foreach ($overpassQueryAdditionalElements as $element) {
            $elementRemote['joinAreas'][] = $element;
          }
        }
      }

      $entity = $entityStorage->load($id);

      if (!$entity) {
        $entity = $entityStorage->create(['id' => $id]);
      }

      if ($entityTypeData->parentField) {
        $elementRemote['customData'] = [
          $entityTypeData->parentField => $syncOptions->parentId,
        ];
      }

      $entityChanges = new EntityTrackChanges($entity);
      self::entityFillFromOsm($entity, $elementRemote);
      if ($entityChanges->isChanged($entity)) {
        if ($entity->isNew()) {
          $result->created++;
        }
        else {
          $result->updated++;
        }
        $entity->save();
      }
      // To cleanup old heavy geometry objects from memory.
      $elementRemote = NULL;
      $entity = NULL;
      $entityStorage->resetCache();
      $entityGeometryStorage->resetCache();
      Utils::forceMemClean();

      $elementsProcessedIds[] = $id;
      $result->processed++;
    }
    $elementsRemoteReader->close();
    unset($elementsRemoteReader);
    unlink($tmpFilePath);
    Utils::forceMemClean();

    // Creating element from parent area, if emptyChildrenUseParent mode is enabled.
    if (
      $syncOptions->mode == 'resync'
      && $entityTypeData->emptyChildrenUseParent === TRUE
      && count($elementsProcessedIds) == 0
    ) {
      if ($parentEntity = \Drupal::entityTypeManager()->getStorage($entityTypeData->parentEntityType)->load($syncOptions->parentId)) {

        $overpassGeneratedElement = [
          'id' => $parentEntity->id(),
          'tags' => [
            'name' => \Drupal::service('string_translation')->translate('Area of @name', [
              '@name' => $parentEntity->label(),
            ]),
          ],
          'boundary' => $parentEntity->boundary,
          'boundary_s' => $parentEntity->boundary_s,
          'isParentArea' => TRUE,
        ];
      }
      if ($entityTypeData->parentField) {
        $overpassGeneratedElement['customData'] = [
          $entityTypeData->parentField => $syncOptions->parentId,
        ];
      }

      $entity = $entityStorage->load($overpassGeneratedElement[$id]);

      if (!$entity) {
        $entity = $entityStorage->create(['id' => $overpassGeneratedElement[$id]]);
      }
      $entityChanges = new EntityTrackChanges($entity);
      self::entityFillFromOsm($entity, $overpassGeneratedElement);
      if ($entityChanges->isChanged($entity)) {
        if ($entity->isNew()) {
          $result->created++;
        }
        else {
          $result->updated++;
        }
        $entity->save();
      }
      $elementsProcessedIds[] = $id;
      $result->processed++;
    }

    if (!empty($elementsProcessedIds)) {
      $result->idLast = end($elementsProcessedIds);
    }

    /* Deleting missing entities start. */
    if ($syncOptions->mode == 'resync') {
      $entitiesToDeleteQuery = $entityStorage->getQuery();
      if (!empty($elementsProcessedIds)) {
        $entitiesToDeleteQuery->condition('id', $elementsProcessedIds, 'NOT IN');
      }
      if (count($elementsProcessedIds) < $syncOptions->limit) {
        $entitiesToDeleteQuery->condition('id', $syncOptions->fromId, ">=");
      }
      else {
        $entitiesToDeleteQuery->condition('id', [$syncOptions->fromId, $result->idLast], 'BETWEEN');
      }

      if (!empty($syncOptions->parentId ?? NULL)) {
        $entitiesToDeleteQuery->condition($entityTypeData->parentField, $syncOptions->parentId);
      }

      $entitiesLocalToDelete = $entityStorage->loadMultiple($entitiesToDeleteQuery->execute());
      $deleted = 0;
      if ($entitiesLocalToDelete) {
        foreach ($entitiesLocalToDelete as $entityLocalToDelete) {
          $entityLocalToDelete->delete();
          $deleted++;
        }
      }
    }
    /* Deleting missing entities end. */

    // Cleaning up memory.
    $entityStorage->resetCache();
    $entityGeometryStorage->resetCache();

    $result->duration = \Drupal::time()->getCurrentMicroTime() - $microtimeStart;

    return $result;

  }

  /**
   * Loads custom override rules for sync process from module config.
   */
  public static function getOverrideRules(string $entityType) {
    static $overrideRules;
    if (!is_array($overrideRules)) {
      $overrideRules = Yaml::parse(\Drupal::config('osm_localities.settings')->get('override_rules'));
    }
    return $overrideRules[$entityType] ?? NULL;
  }

  /**
   * Helper: Adds indent to multiline string.
   */
  public static function indent(string $str, int $spaces) {
    $parts = array_filter(explode("\n", $str));
    $parts = array_map(function ($part) use ($spaces) {
      return str_repeat(' ', $spaces) . $part;
    }, $parts);
    return implode("\n", $parts);
  }

  /**
   * Attachs common fields from Overpass geom data to osm_localities entities.
   */
  public static function entityFillFromOsm(FieldableEntityInterface &$entity, array $element) {
    $entityType = $entity->getEntityTypeId();
    $entityTypeData = self::getEntityTypeData($entityType);

    $entity->name = OsmLocalities::getPrefferedNameFromTags('name', $element['tags']);
    if ($entityTypeData->parentField && $element['customData'][$entityTypeData->parentField]) {
      $entity->{$entityTypeData->parentField} = $element['customData'][$entityTypeData->parentField];
    }
    $entity->wikidata_id = $element['tags']['wikidata'] ?? NULL;

    if ($entity->hasField('wikidata_id')) {
      $entity->wikidata_id = $element['tags']['wikidata'] ?? NULL;
    }

    if ($entity->hasField('admin_center')) {
      $adminCenters = OsmApi::osmRelationFilterMembersByRole(
        $element,
        ['admin_centre', 'admin_center'],
        'node', TRUE
      );
      $entity->admin_center = $adminCenters[0]->ref ?? NULL;
    }

    if ($entity->hasField('coordinates')) {
      // Trying to find center from OSM relation object.
      if (
        ($node = OsmApi::osmRelationFilterMembersByRole(
          $element,
          ['label'],
          'node', TRUE
        ))
        && isset($node['lon'])
      ) {
        $entity->coordinates = "POINT ({$node['lon']} {$node['lat']})";
      }
      elseif (
        ($node = OsmApi::osmRelationFilterMembersByRole(
          $element,
          ['admin_centre', 'admin_center'],
          'node', TRUE
        ))
        && isset($node['lon'])
      ) {
        $entity->coordinates = "POINT ({$node['lon']} {$node['lat']})";
      }
      elseif (isset($element['center']['lon'])) {
        // For node type of OSM Element.
        $entity->coordinates = "POINT ({$element['center']['lon']} {$element['center']['lat']})";
      }
      elseif (isset($element['lon'])) {
        // For node type of OSM Element.
        $entity->coordinates = "POINT ({$element['lon']} {$element['lat']})";
      }
      else {
        // @todo Fill as center of area
      }
    }
    $entityGeometryStorage = \Drupal::entityTypeManager()->getStorage('osm_geometry');
    if ($entity->hasField('boundary')) {
      try {
        $boundaryGeoPhp = OsmApi::relationToGeophp($element);

        if ($element['joinAreas'] ?? NULL) {
          foreach ($element['joinAreas'] as $elementToJoin) {
            if ($boundaryJoinGeoPhp = OsmApi::relationToGeophp($elementToJoin)) {
              if ($boundaryGeoPhp && Utils::isGeosAvailable()) {
                $boundaryGeoPhp = $boundaryGeoPhp->union($boundaryJoinGeoPhp);
              }
              else {
                $boundaryGeoPhp = $boundaryJoinGeoPhp;
              }
              unset($boundaryJoinGeoPhp);
            }
          }
        }
        if ($boundaryGeoPhp) {
          if ($entity->boundary->isEmpty()) {
            $entity->boundary = $entityGeometryStorage->create();
          }
          $boundaryPrevMd5 = md5($entity->boundary->entity->geometry->value);
          try {
            $boundaryWkt = $boundaryGeoPhp->out('wkt');
          }
          catch (\Exception $e) {
            // Skipping broken boundary.
            $boundaryWkt = '';
          }
          if (md5($boundaryWkt) !== $boundaryPrevMd5) {
            $entity->boundary->entity->geometry = $boundaryWkt;
            $entity->boundary->entity->save();
          }
          $boundaryWkt = NULL;
          $entityGeometryStorage->resetCache();

          if ($entity->hasField('boundary_s')) {
            if (Utils::isGeosAvailable()) {
              $boundaryGeoPhpSimplified = OsmApi::simplifyPolygon($boundaryGeoPhp, 1);
            }
            else {
              $boundaryGeoPhpSimplified = $boundaryGeoPhp;
            }
            if ($entity->boundary_s->isEmpty()) {
              $entity->boundary_s = $entityGeometryStorage->create();
            }
            $boundaryPrevMd5 = md5($entity->boundary_s->entity->geometry->value);
            try {
              $boundaryWkt = $boundaryGeoPhpSimplified->out('wkt');
            }
            catch (\Exception $e) {
              // Skipping broken boundary.
              $boundaryWkt = '';
            }
            if (md5($boundaryWkt) !== $boundaryPrevMd5) {
              $entity->boundary_s->entity->geometry = $boundaryWkt;
              $entity->boundary_s->entity->save();
            }
          }
        }
      }
      catch (\Exception $e) {
        // Ignoring failed boundary generation.
      }
    }

    // Per entity type fields.
    // osm_country.
    if ($entityType == 'osm_country') {
      $entity->code = $element['tags']['ISO3166-1:alpha2'] ?? $element['tags']['ISO3166-1'] ?? NULL;
    }

    // osm_locality.
    elseif ($entityType == 'osm_locality') {
      $entity->population = $element['tags']['population'] ?? NULL;
      $entity->type = $element['tags']['place'] ?? NULL;
      $countyEntity = \Drupal::entityTypeManager()->getStorage('osm_county')->load($element['customData'][$entityTypeData->parentField]);
      $entity->region = $countyEntity->region->target_id;
      $entity->country = $countyEntity->region->entity->country->target_id;
      $addressParts = [
        $countyEntity->region->entity->country->entity->label(),
        $countyEntity->region->entity->is_parent_area ? $countyEntity->region->entity->label() : NULL,
        $countyEntity->is_parent_area ? $countyEntity->label() : NULL,
      ];
      $entity->address = implode(', ', array_filter($addressParts));

      $entity->is_county_center = ($element['id'] == $countyEntity->admin_center->target_id);
      $entity->is_region_center = ($element['id'] == $countyEntity->region->entity->admin_center->target_id);
      $entity->is_capital = ($element['id'] == $countyEntity->region->entity->country->entity->admin_center->target_id);
    }

  }

}
