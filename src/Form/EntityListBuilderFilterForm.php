<?php

namespace Drupal\osm_localities\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\osm_localities\OsmLocalities;

/**
 * Configure dt1 settings for this site.
 */
class EntityListBuilderFilterForm extends FormBase implements FormInterface {

  /**
   * Entity type to build form for.
   *
   * @var string
   */
  protected $entityType;

  /**
   * Custom data of entity type.
   *
   * @var Drupal\osm_localities\DTO\OsmLocalitiesEntityTypeData
   */
  protected $entityTypeData;

  /**
   * Form request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'osm_localities_entity_list_filter';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $this->entityType = $form_state->getBuildInfo()['args'][0];
    $this->entityTypeData = OsmLocalities::getEntityTypeData($this->entityType);
    $this->request = \Drupal::request();

    $fieldDefinitions = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($this->entityType, $this->entityType);

    $form['filter'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form--inline', 'clearfix'],
      ],
    ];
    if ($this->entityTypeData->parentField) {
      $form['filter'][$this->entityTypeData->parentField] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => $this->entityTypeData->parentEntityType,
        '#title' => $fieldDefinitions[$this->entityTypeData->parentField]->getLabel(),
      ];
      if ($parentId = $this->request->get($this->entityTypeData->parentField)) {
        $form['filter'][$this->entityTypeData->parentField]['#default_value'] =
        \Drupal::entityTypeManager()
          ->getStorage($this->entityTypeData->parentEntityType)
          ->load($parentId);
      }
    }
    $form['filter']['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#default_value' => $this->request->get('name') ?? '',
    ];
    if ($parentId = $this->request->get($this->entityTypeData->parentField)) {
      $form['filter'][$this->entityTypeData->parentField]['#default_value'] =
        \Drupal::entityTypeManager()
          ->getStorage($this->entityTypeData->parentEntityType)
          ->load($parentId);
    }

    if ($this->entityType == 'osm_locality') {
      $form['filter']['is_county_center'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('County centers'),
        '#default_value' => $this->request->get('is_county_center') ?? FALSE,
      ];
      $form['filter']['is_region_center'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Region centers'),
        '#default_value' => $this->request->get('is_region_center') ?? FALSE,
      ];
      $form['filter']['is_capital'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Capitals'),
        '#default_value' => $this->request->get('is_capital') ?? FALSE,
      ];

    }

    $form['actions']['wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['form-item']],
    ];

    $form['actions']['wrapper']['submit'] = [
      '#type' => 'submit',
      '#value' => 'Filter',
    ];

    if ($this->request->getQueryString()) {
      $form['actions']['wrapper']['reset'] = [
        '#type' => 'submit',
        '#value' => 'Reset',
        '#submit' => ['::resetForm'],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $query = [];

    if ($parentId = $form_state->getValue($this->entityTypeData->parentField)) {
      $query[$this->entityTypeData->parentField] = $parentId;
    }
    if ($value = $form_state->getValue('name')) {
      $query['name'] = $value;
    }
    if ($value = $form_state->getValue('is_county_center')) {
      $query['is_county_center'] = $value;
    }
    if ($value = $form_state->getValue('is_region_center')) {
      $query['is_region_center'] = $value;
    }
    if ($value = $form_state->getValue('is_capital')) {
      $query['is_capital'] = $value;
    }
    $form_state->setRedirect('entity.' . $this->entityType . '.collection', $query);
  }

  /**
   * {@inheritdoc}
   */
  public function resetForm(array $form, FormStateInterface &$form_state) {
    $form_state->setRedirect('entity.' . $this->entityType . '.collection');
  }

}
