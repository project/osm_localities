<?php

namespace Drupal\osm_localities\Form;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\osm_localities\Utils;
use Symfony\Component\Yaml\Yaml;

/**
 * Configure osm_localities settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'osm_localities_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['osm_localities.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Checking availability of php-geos library.
    if (!Utils::isGeosAvailable()) {
      \Drupal::messenger()->addWarning(new FormattableMarkup('<strong>@title</strong>! @description', [
        '@title' => $this->t('Missing PHP GEOS library'),
        '@description' => $this->t('Library <a href="@url">php-geos</a> is highly recommended to simplify OSM polygons, that significantly decreases usage of memory to render polygons. Install it and do a full resync of data afterwise.', ['@url' => 'https://github.com/libgeos/php-geos']),
      ]));
    }

    $config = $this->config('osm_localities.settings');
    $form['languages'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Languages'),
      '#description' => $this->t('Comma-separated list of preferred languages (2 letters, without spaces) for OSM object names. Use <code>default</code> for default language of OSM entity. Leave empty to use object names in site language, then EN and default as fallback. Example: <code>ru,en,default,fr</code>'),
      '#default_value' => $config->get('languages'),
    ];

    $form['locality_types'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Types of locality to sync'),
      '#options' => [
        'city' => $this->t('City'),
        'town' => $this->t('Town'),
        'village' => $this->t('Village'),
      ],
      '#description' => $this->t('Select locality types to sync from OSM database.'),
      '#default_value' => $config->get('locality_types'),
    ];

    $form['chunk_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Amount of items to sync in one operation'),
      '#min' => 1,
      '#required' => TRUE,
      '#description' => $this->t('Larger number increase the sync process speed and decrease number of calls to Overpass API, but may cause memory limit problems.'),
      '#default_value' => $config->get('chunk_size'),
    ];

    $form['chunk_size_countries'] = [
      '#type' => 'number',
      '#title' => $this->t('Amount of items to sync for Countries query'),
      '#min' => 1,
      '#required' => TRUE,
      '#description' => $this->t('Countries contains large amount of data, eg 100 first countres returns JSON with size more than 200mb. So we use separate chunk size for countries.'),
      '#default_value' => $config->get('chunk_size_countries'),
    ];

    $form['background_sync_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use background sync'),
      '#description' => $this->t('Enables periodical sync of data by cron job'),
      '#default_value' => $config->get('background_sync.enabled'),
    ];

    $form['background_sync_steps'] = [
      '#type' => 'number',
      '#title' => $this->t('Steps per one cron launch'),
      '#description' => $this->t('Number of steps to execute per one cron operation. 1 is optimal, higher values can increase resync speed but can give timeout and rate limit errors.'),
      '#default_value' => $config->get('background_sync.steps'),
    ];

    $form['background_sync_timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Minimal seconds between cron jobs'),
      '#description' => $this->t('Minimal amount of seconds between cron jobs, helps preventing rate limit problems.'),
      '#default_value' => $config->get('background_sync.timeout'),
    ];

    if (
      ($syncCronState = \Drupal::state()->get('osm_localities.sync_cron.state'))
      && ($syncCronStat = \Drupal::state()->get('osm_localities.sync_cron.stat'))
    ) {
      $form['background_sync_info'] = [
        '#markup' => $this->t('Last backround sync executed at @timestamp within @duration sec.', [
          '@timestamp' => \Drupal::service('date.formatter')->format($syncCronState->timestamp),
          '@duration' => round($syncCronStat->duration, 2),
        ]),
      ];
      if (\Drupal::service('lock.persistent')->lockMayBeAvailable('osm_localities.sync_cron.running') == FALSE) {
        $form['background_sync_info']['#markup'] .= ' ' . $this->t('Cron job is executing now.');
      }
      if ($lockExpire = Utils::getPersistentLockExpireTimestamp('osm_localities.sync_cron.timeout')
        ) {
        $form['background_sync_info']['#markup'] .= ' ' . $this->t('Timeout active until @timestamp.', [
          '@timestamp' => \Drupal::service('date.formatter')->format((int) $lockExpire),
        ]);
      }
      if ($syncCronState->idLast == 0) {
        $syncPosition = 0;
      }
      else {
        $entityTypeCountTotal = \Drupal::entityQuery($syncCronState->entityType)
          ->count()
          ->execute();
        $entityTypeCountSynced = \Drupal::entityQuery($syncCronState->entityType)
          ->condition('id', $syncCronState->idLast, '<=')
          ->count()
          ->execute();
        if ($entityTypeCountSynced > 0) {
          $syncPosition = floor($entityTypeCountSynced / $entityTypeCountTotal * 100);
        }
        else {
          $syncPosition = 0;
        }
        $form['background_sync_details'] = [
          '#markup' => ' ' . $this->t('Current state: @mode @entityTypeLabel at @position%. Processed @processed items, @changed changed, last id: @idLast.', [
            '@mode' => $syncCronState->mode,
            '@entityTypeLabel' => \Drupal::entityTypeManager()
              ->getStorage($syncCronState->entityType)
              ->getEntityType()
              ->getLabel('collection'),
            '@position' => $syncPosition,
            '@changed' => $syncCronStat->changed(),
            '@processed' => $syncCronStat->processed,
            '@idLast' => $syncCronState->idLast ?? '-',
          ]),
        ];
      }
    }

    $form['advanced'] = [
      '#type' => 'details',
      '#title' => $this->t('Advanced'),
      '#collapsible' => TRUE,
      '#open' => !empty($overrideRules),
    ];

    $overrideRules = $config->get('override_rules');
    $form['advanced']['override_rules'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Override rules'),
      '#default_value' => $overrideRules,
      '#description' => 'List of rules in YAML format to ignore and merge OSM items',
    ];

    $form['advanced']['override_rules_example'] = [
      '#type' => 'details',
      '#title' => $this->t('Override rules example'),
      '#collapsible' => TRUE,
      '#open' => FALSE,
    ];

    $form['advanced']['override_rules_example']['example'] = [
      '#markup' => '
<pre>
osm_country:
  59065: # Belarus
    children:
      ignore:
        - 59195    # Minsk
osm_region:
  59752:           # Minsk region
    joinAreas:
      - 59195      # Joining Minsk area to Minsk region for search of countys
    children:      # Adds children items manually by id
      add:
        - 59195    # Joining Minsk as county, because of missing countys
      ignore:
        - 59752    # Ignoring Minsk region
</pre>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    try {
      Yaml::parse($form_state->getValue('override_rules'));
    }
    catch (\Exception $e) {
      $form_state->setErrorByName('override_rules', $this->t($e->getMessage()));
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('osm_localities.settings')
      ->set('languages', $form_state->getValue('languages'))
      ->set('locality_types', $form_state->getValue('locality_types'))
      ->set('override_rules', $form_state->getValue('override_rules'))
      ->set('chunk_size', $form_state->getValue('chunk_size'))
      ->set('chunk_size_countries', $form_state->getValue('chunk_size_countries'))
      ->set('background_sync.enabled', $form_state->getValue('background_sync_enabled'))
      ->set('background_sync.steps', $form_state->getValue('background_sync_steps'))
      ->set('background_sync.timeout', $form_state->getValue('background_sync_timeout'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
