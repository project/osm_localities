<?php

namespace Drupal\osm_localities;

use Drupal\osm_localities\DTO\OsmLocalitiesSyncOptions;
use Drupal\overpass_api\Exception\OverpassApiHttpException;

/**
 * Performs batch sync of osm entities.
 */
class OsmSyncBatch {

  /**
   * Processor for batch operations.
   */
  public static function process(string $entityType, string $mode, int $parentId = NULL, array &$context) {

    // Initializing the process and filling default values.
    if (!isset($context['sandbox']['total'])) {
      $context['sandbox']['processed'] = 0;

      $context['sandbox']['stat'] = [
        'created' => 0,
        'updated' => 0,
        'deleted' => 0,
      ];
      $context['sandbox']['sync_options'] = new OsmLocalitiesSyncOptions([
        'entityType' => $entityType,
        'mode' => $mode,
        'limit' => $entityType == 'osm_country'
        ? (int) \Drupal::config('osm_localities.settings')->get('chunk_size_countries') ?? 10
        : (int) \Drupal::config('osm_localities.settings')->get('chunk_size') ?? 100,
      ]);
      if ($mode == 'update') {
        $syncFromTime = OsmLocalities::getEntityTypeSyncTimestamp($entityType);
        if ($syncFromTime > 0) {
          $context['sandbox']['sync_options']->filter = '(newer:"' . date('c', $syncFromTime) . '")';
        }
      }

      $context['sandbox']['idLast'] = 0;

      $context['results']['data']['startTime'] = time();
      $context['results']['data']['entityType'] = $entityType;
      $context['results']['data']['entityTypeLabel'] =
        \Drupal::entityTypeManager()
          ->getStorage($entityType)
          ->getEntityType()
          ->getLabel();

      if ($parentId !== NULL) {
        $context['sandbox']['sync_options']->parentId = $parentId;
        $entityTypeData = OsmLocalities::getEntityTypeData($entityType);
        $context['results']['data']['parentEntityTypeLabel'] =
          \Drupal::entityTypeManager()
            ->getStorage($entityTypeData->parentEntityType)
            ->getEntityType()
            ->getLabel();
        try {
          $parentEntity = \Drupal::entityTypeManager()
            ->getStorage($entityTypeData->parentEntityType)
            ->load($parentId);
          if ($parentEntity) {
            $context['results']['data']['parentEntityId'] = $parentEntity->id();
            $context['results']['data']['parentEntityLabel'] = $parentEntity->label();
          }
          else {
            $context['results']['data']['parentEntityLabel'] = 'n/a';
          }
        }
        catch (\Exception $e) {
          $context['results']['data']['parentEntityLabel'] = '[deleted]';
        }
      }

      $context['results']['mode'] = $mode;

      $context['finished'] = 0;

    }

    try {
      if (isset($context['sandbox']['idLast'])) {
        $context['sandbox']['sync_options']->fromId = $context['sandbox']['idLast'] + 1;
      }
      $result = OsmLocalities::osmEntityTypeSyncChunk($context['sandbox']['sync_options']);
      $context['sandbox']['idLast'] = $result->idLast ?? FALSE;
      $context['sandbox']['processed'] += $result->processed;
      if ($result->processed > 0) {
        $context['sandbox']['stat']['created'] += $result->created;
        $context['sandbox']['stat']['updated'] += $result->updated;
        $context['sandbox']['stat']['deleted'] += $result->deleted;
      }
      $context['message'] = t(
      'Processed :processed :totalPhrase :entityTypeLabel items :parentEntityTypeLabelPhrase <span title="id :parentEntityId">:parentEntityLabel</span> (changes: :created created, :updated updated, :deleted deleted).', [
        ':entityTypeLabel' => $context['results']['data']['entityTypeLabel'],
        ':parentEntityTypeLabelPhrase' =>
        ($context['results']['data']['parentEntityTypeLabel'] ?? NULL)
        ? ' ' . t('in') . ' ' . $context['results']['data']['parentEntityTypeLabel']
        : '',
        ':parentEntityId' => $context['results']['data']['parentEntityId'] ?? '',
        ':parentEntityLabel' => $context['results']['data']['parentEntityLabel'] ?? '',
        ':processed' => $context['sandbox']['processed'],
        ':totalPhrase' =>
        ($context['sandbox']['total'] ?? NULL)
        ? ' ' . t('of') . ' ' . $context['sandbox']['total']
        : '',
        ':created' => $context['sandbox']['stat']['created'],
        ':updated' => $context['sandbox']['stat']['updated'],
        ':deleted' => $context['sandbox']['stat']['deleted'],
      ]);
    }
    catch (OverpassApiHttpException $e) {
      $context['message'] = t(
      "Error retrieving data from Overpass: :exception", [
        ':exception' => $e->getMessage(),
      ])
        . ' - ' . t("Repeating") . '...';
      return;
    }
    if (!isset($context['sandbox']['total'])) {
      if ($context['sandbox']['processed'] < $context['sandbox']['sync_options']->limit) {
        $context['sandbox']['total'] = $context['sandbox']['processed'];
      }
      else {
        $context['sandbox']['total'] = OsmLocalities::osmEntityTypeRemoteCount($context['sandbox']['sync_options']);
      }
    }

    $context['finished'] = $context['sandbox']['total'] == 0
    ? 1
    : $context['sandbox']['processed'] / $context['sandbox']['total'];

    if ($context['finished'] >= 1) {
      $context['results']['stat'] = $context['sandbox']['stat'];
      $context['results']['stat']['processed'] = $context['sandbox']['processed'];
    }
    else {
      $context['message'] =
      ($context['message'] ?? '')
      . ' ' . t('Now processing next :chunkSize items...', [
        ':chunkSize' => $context['sandbox']['sync_options']->limit,
      ]);
    }
  }

  /**
   * Finished callback for batch.
   */
  public static function finished($success, $results, $operations) {
    if (($results['stat']['processed'] ?? 0) > 0) {
      $message = t('Full resync completed successfully, processed :processed items.', [
        ':processed' => $results['stat']['processed'] ?? 0,
      ]);
      if (isset($results['stat']['created'])) {
        $message .= ' ' . t('Statistics: :created created, :updated updated, :deleted deleted.
        ', [
          ':created' => $results['stat']['created'],
          ':updated' => $results['stat']['updated'],
          ':deleted' => $results['stat']['deleted'],
        ]);
      }

      OsmLocalities::setEntityTypeSyncTimestamp($results['data']['entityType'], $results['mode'], $results['data']['startTime']);
    }
    else {
      $message = 'There is no items with sync enabled to process.';
    }
    \Drupal::messenger()->addStatus($message);
  }

}
