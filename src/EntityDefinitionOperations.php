<?php
namespace Drupal\osm_localities;

use Drupal\Core\Field\FieldStorageDefinitionInterface;

class EntityDefinitionOperations {
  public static function entityDefinitionFieldAction(string $entityType, string $fieldName, string $action = 'install', bool $ignoreErrors = false) {
    $moduleName = 'osm_localities';
    $entityDefinitionUpdateManager = \Drupal::entityDefinitionUpdateManager();
    $fieldDefinitions = \Drupal::service('entity_field.manager')
      ->getFieldDefinitions($entityType, $entityType);
    $fieldDefinitionsInstalled = \Drupal::service('entity.last_installed_schema.repository')
      ->getLastInstalledFieldStorageDefinitions($entityType);

    switch($action) {
      case "install":
        if (empty($fieldDefinitions[$fieldName]) && !$fieldDefinitions[$fieldName] instanceof FieldStorageDefinitionInterface) {
          if(!$ignoreErrors) throw new \Exception("Field $fieldName on $entityType is not exists, try to clear the cache and retry");
          return;
        }
        return $entityDefinitionUpdateManager
          ->installFieldStorageDefinition(
            $fieldName,
            $entityType,
            $moduleName,
            $fieldDefinitions[$fieldName]);
      case "update":
        if (empty($fieldDefinitions[$fieldName]) && !$fieldDefinitions[$fieldName] instanceof FieldStorageDefinitionInterface) {
          if(!$ignoreErrors) throw new \Exception("Field $fieldName on $entityType is not exists, try to clear the cache and retry");
          return;
        }
        if (empty($fieldDefinitionsInstalled[$fieldName])) {
          if(!$ignoreErrors) throw new \Exception("Field $fieldName on $entityType is not installed, try to install first");
          return;
        }
        return $entityDefinitionUpdateManager->updateFieldStorageDefinition($fieldDefinitions[$fieldName]);

      case "uninstall":
        if (empty($fieldDefinitionsInstalled[$fieldName])) {
          if(!$ignoreErrors) throw new \Exception("Field $fieldName on $entityType is not installed, try to install first");
          return;
        }
        return $entityDefinitionUpdateManager
          ->uninstallFieldStorageDefinition($fieldDefinitionsInstalled[$fieldName]);
      default:
        throw new \Exception("Bad action type: $action");
    }
  }

  /**
   *
   */
  public static function osmEntityTypeUpdate(string $entityType) {

    // Updating entity definition
    $entityTypeDefinition = \Drupal::service('entity_type.manager')->getDefinition($entityType);
    \Drupal::entityDefinitionUpdateManager()->updateEntityType($entityTypeDefinition);

    // Synchroniznig base fields
    $fieldDefinitions = \Drupal::service('entity_field.manager')
    ->getFieldDefinitions($entityType, $entityType);

    $fieldDefinitionsInstalled = \Drupal::service('entity.last_installed_schema.repository')
      ->getLastInstalledFieldStorageDefinitions($entityType);

    $entityDefinitionUpdateManager = \Drupal::entityDefinitionUpdateManager();

    $fieldsToInstall = array_diff(array_keys($fieldDefinitions), array_keys($fieldDefinitionsInstalled));
    $fieldsToUninstall = array_diff(array_keys($fieldDefinitionsInstalled), array_keys($fieldDefinitions));


    foreach($fieldsToInstall as $field) {
      self::entityDefinitionFieldAction($entityType, $field, 'install');
    }
    foreach($fieldsToUninstall as $field) {
      self::entityDefinitionFieldAction($entityType, $field, 'uninstall');
    }
  }

}