<?php

namespace Drupal\osm_localities;

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Various utility functions.
 */
class Utils {

  /**
   * Gets lock expire timestamp by id.
   *
   * @param bool $lockName
   *   Name of the lock.
   */
  public static function getPersistentLockExpireTimestamp(string $lockName) {
    return \Drupal::database()->select('semaphore', 's')
      ->fields('s', ['expire'])
      ->condition('s.name', $lockName)
      ->execute()->fetchField();
  }

  /**
   * Checks availability of php-geos library.
   *
   * @param bool $throwException
   *   If true - throws an exception instead of return value.
   */
  public static function isGeosAvailable($throwException = FALSE) {
    $availability = class_exists('GEOSGeometry');
    if ($throwException) {
      throw new \Exception('php-geos library is not installed! It is needed to simplify polygons and join areas.');
    }

    return $availability;
  }

  /**
   * Gets PHP memory limit.
   */
  public static function getMemoryLimit() {
    return self::returnBytes(ini_get('memory_limit'));
  }

  /**
   * Converts shorthand memory notation value to bytes
   * From http://php.net/manual/en/function.ini-get.php
   *
   * @param string $val
   *   Memory size shorthand notation string.
   */
  public static function returnBytes(string $val) {
    $val = trim($val);
    $last = strtolower($val[strlen($val) - 1]);
    $val = substr($val, 0, -1);
    switch ($last) {
      // The 'G' modifier is available since PHP 5.1.0.
      case 'g':
        $val *= 1024;
      case 'm':
        $val *= 1024;
      case 'k':
        $val *= 1024;
    }
    return $val;
  }

  /**
   * Tries to extend PHP memory and time limits if allowed.
   */
  public static function extendLimits() {
    function_exists('ini_set') && ini_set('memory_limit', '512M');
    function_exists('ini_set') && function_exists('set_time_limit') && set_time_limit('1200');
  }

  /**
   * Forces garbage collectors to clear memory.
   */
  public static function forceMemClean() {
    gc_enable();
    gc_mem_caches();
    gc_collect_cycles();
  }

  /**
   * Returns an entity_reference field.
   *
   * @param string $targetEntityTypeId
   *   String with entity type ID.
   * @param _label|_label_link|string $viewMode
   *   A _label or _label_link for display label, or view mode id, eg 'default'.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition
   *   A base field definition.
   */
  public static function entityReferenceBaseFieldDefinition(string $targetEntityTypeId, string $viewMode = '_label_link') {
    $fieldDefinition = BaseFieldDefinition::create('entity_reference')
      ->setSetting('target_type', $targetEntityTypeId)
      ->setSetting('handler', 'default');

    if ($viewMode == '_label' || $viewMode == '_label_link') {
      $fieldDefinition->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'settings' => [
          'link' => $viewMode == '_label_link' ? TRUE : FALSE,
        ],
      ]);
    }
    else {
      $fieldDefinition->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_entity_view',
        'settings' => [
          'view_mode' => $viewMode,
        ],
      ]);
    }

    $fieldDefinition->setDisplayOptions('form', [
      'type' => 'entity_reference_autocomplete',
      'settings' => [
        'match_operator' => 'CONTAINS',
        'size' => '60',
        'placeholder' => '',
      ],
    ]);

    return $fieldDefinition;
  }

  /**
   * Returns display view options for geofield as map.
   */
  public static function geofieldDisplayOptionsViewMap() {
    return [
      'label' => 'above',
      'type' => 'leaflet_formatter_default',
      'settings' => [
        'disable_wheel' => TRUE,
    // leaflet_map: 'OSM Mapnik'.
      ],
    ];
  }

  /**
   * Returns display edit options for geofield as map.
   */
  public static function geofieldDisplayOptionsEditMap() {
    return [
      'type' => 'geolocation_leaflet',
      'settings' => [
        'auto_client_location' => '',
        'auto_client_location_marker' => '',
      ],
    ];
  }

}
