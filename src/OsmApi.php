<?php

namespace Drupal\osm_localities;

use Symfony\Component\Process\Exception\RuntimeException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

/**
 * Helper functions for working with OSM and Overpass objects.
 */
class OsmApi {

  /**
   * Searches members in OSM element by needed role and optionally type.
   */
  public static function osmRelationFilterMembersByRole(array $element, array $roles, string $type = NULL, bool $returnFirst = FALSE) {
    $matched = [];
    foreach ($element['members'] ?? [] as $member) {
      if ($type && ($member['type'] ?? NULL) != $type) {
        continue;
      }
      if (in_array($member['role'], $roles)) {
        if ($returnFirst) {
          return $member;
        }
        $matched[] = $member;
      }
    }
    return $matched;
  }

  /**
   * Simplifies polygon by levels.
   *
   * @param \geoPHP\Geometry\Geometry $polygon
   *   GeoPHP class with polygon.
   * @param int $level
   *   Level of simlifying: 1 or 2.
   * @param \geoPHP\Geometry\Geometry $polygonFallback
   *   Polygon to return, if passed polygon after simplifying disappears.
   *   If missing, main passed polygon is returned.
   *
   * @return \geoPHP\Geometry\Geometry
   *   Simplified geometry, or fallback/original geometry if simplify fails.
   */
  public static function simplifyPolygon($polygon, int $level, $polygonFallback = NULL) {
    $simplifyTolerancesByArea = [
      "100" => [0.1, 0.02],
      "10" => [0.04, 0.008],
      "0.1" => [0.02, 0.004],
      "0.05" => [0.01, 0.002],
      "0.01" => [0.001, 0.0002],
      "0" => [0, 0],
    ];
    try {
      $polygonArea = $polygon->area();
      foreach ($simplifyTolerancesByArea as $areaSize => $simplifyToleranceTesting) {
        if ($polygonArea > $areaSize) {
          $simplifyTolerance = $simplifyToleranceTesting;
          break;
        }
      }
      $tolerance = $simplifyTolerance[$level - 1];
      $polygonS = $polygon->simplify($tolerance);
      if (count($polygonS->components ?? []) < 1) {
        $polygonS = $polygonFallback ?? $polygon;
      }
    }
    catch (\Exception $e) {
      $polygonS = $polygonFallback ?? $polygon;

    }
    // $countOrig = 0;
    // foreach ($polygon->components as $component) {
    //   $countOrig += count($component->components[0]->components);
    // }
    // $countS = 0;
    // foreach ($polygonS->components as $component) {
    //   $countS += count($component->components[0]->components);
    // }
    // dpm("tolerance level $level - $tolerance; count $countOrig > $countS");
    return $polygonS;
  }

  /**
   * Converts Osm Relation with geom to geoPHP.
   *
   * @param array $element
   *   OSM relation element as Object from JSON.
   *
   * @return \geoPHP\Geometry\Geometry
   *   geoPHP with geometry of relation from OSM element.
   */
  public static function relationToGeophp(array $element) {
    $elementAsOverpassReply = [
      'elements' => [$element],
    ];
    $osmGeoJson = self::osmToGeojson($elementAsOverpassReply);
    if (isset($osmGeoJson->type)) {
      $geoPhp = \Drupal::service('geofield.geophp')->load(json_encode($osmGeoJson));
      return $geoPhp;
    }
    foreach ($osmGeoJson->features as $feature) {
      if ($feature->id == 'relation/' . $element->id) {
        $geoJson = $feature->geometry;
        $geoPhp = \Drupal::service('geofield.geophp')->load(json_encode($geoJson));
        return $geoPhp;
      }
    }
    throw new \Exception('Relation data not found');
  }

  /**
   * Converts members OSM (Overpass) results to GeoJSON format.
   *
   * @param array $element
   *   OSM relation element as Object from JSON.
   *
   * @return object
   *   OSM element object with GeoJSON formatted members.
   */
  public static function osmToGeojson(array $element) {
    // @todo Implement PHP version of conversion tool
    // JS variant 1: https://github.com/tibetty/osm2geojson-lite
    // JS variant 2: https://github.com/tyrasd/osmtogeojson
    $process = new Process(['./osm2geojson-lite'], drupal_get_path('module', 'osm_localities') . '/assets/osm2geojson-lite');
    $process->setInput(json_encode($element));
    try {
      $process->run();
      if ($process->getExitCode() !== 0) {
        $errorOutput = $process->getErrorOutput();
        if (strpos($errorOutput, "'node': No such file or directory")) {
          throw new \Exception('Error launching osm2geojson-lite node.js utility: check if Node.js is installed on your system');
        }
        throw new \Exception('Error getting geometry from OSM element using osm2geojson-lite node.js utility');
      }
      $output = $process->getOutput();
      $elementGeojson = json_decode($output);
    }
    catch (ProcessFailedException | RuntimeException $exception) {
      throw new \Exception('Error launching osm2geojson-lite node.js utility', 0, $exception);
    }
    return $elementGeojson;
  }

}
