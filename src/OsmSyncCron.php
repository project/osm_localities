<?php

namespace Drupal\osm_localities;

use Drupal\osm_localities\DTO\OsmLocalitiesSyncCronState;
use Drupal\osm_localities\DTO\OsmLocalitiesSyncOptions;

/**
 * Performs cron background sync of osm entities.
 */
class OsmSyncCron {
  const ENTITY_TYPES = [
    'osm_country',
    'osm_region',
    'osm_county',
    'osm_locality',
  ];

  /**
   * Processor for cron job.
   */
  public static function cronJob() {
    $config = \Drupal::config('osm_localities.settings');
    $lockService = \Drupal::service('lock.persistent');
    $lockNameRunning = 'osm_localities.sync_cron.running';
    $lockNameTimeout = 'osm_localities.sync_cron.timeout';
    if (!$config->get('background_sync.enabled', FALSE)) {
      return;
    }
    // If ($lockService->lockMayBeAvailable($lockNameRunning) === FALSE) {
    //   return;
    // }
    // if ($lockService->lockMayBeAvailable($lockNameTimeout) === FALSE) {
    //   return;
    // }.
    for ($i = 0; $i <= $config->get('background_sync_steps', 1); $i++) {
      $lockService->acquire($lockNameRunning, $config->get('background_sync.timeout', 600));
      self::processIteration();
      $lockService->release($lockNameRunning);
    }

    if ($timeout = $config->get('background_sync.timeout', FALSE)) {
      $lockService->acquire($lockNameTimeout, $timeout);
    }
  }

  /**
   * Processor for one sync operation.
   */
  public static function processIteration() {
    $microtimeStart = \Drupal::time()->getCurrentMicroTime();
    $state = \Drupal::state();
    $stateKey = 'osm_localities.sync_cron.state';
    $syncCronState = $state->get($stateKey, new OsmLocalitiesSyncCronState());
    $syncCronState->entityType = 'osm_county';
    if ($syncCronState->errorsCount > 10) {
      \Drupal::logger('osm_localities')->error('Cron @mode for @entityType is suspended because of too mutch errors (@count).', [
        '@count' => $syncCronState->errorsCount,
      ]);
      return;
    }

    if (!$syncCronState->entityType) {
      $syncCronState->entityType = self::ENTITY_TYPES[0];
    }
    if (!$syncCronState->timestampStart) {
      $syncCronState->timestampStart = \Drupal::time()->getCurrentTime();
    }

    if (!$syncCronState->mode) {
      $syncCronState->mode = OsmLocalities::getEntityTypeSyncTimestamp($syncCronState->entityType, 'resync') > 0
        ? 'update'
        : 'resync';
    }

    $syncOptions = new OsmLocalitiesSyncOptions([
      'entityType' => $syncCronState->entityType,
      'mode' => $syncCronState->mode,
      'limit' => $syncCronState->entityType == 'osm_country'
      ? (int) \Drupal::config('osm_localities.settings')->get('chunk_size_countries') ?? 10
      : (int) \Drupal::config('osm_localities.settings')->get('chunk_size') ?? 100,
    ]);

    $entityTypeData = OsmLocalities::getEntityTypeData($syncCronState->entityType);

    if ($syncCronState->mode == 'update') {
      $syncFromTime = OsmLocalities::getEntityTypeSyncTimestamp($syncCronState->entityType);
      if ($syncFromTime > 0) {
        $syncOptions->filter = '(newer:"' . date('c', $syncFromTime) . '")';
      }
    }

    if ($syncCronState->idLast) {
      $syncOptions->fromId = $syncCronState->idLast + 1;
    }
    else {
      $syncCronState->idLast = NULL;
    }

    if ($entityTypeData->parentEntityType) {
      $syncOptions->parentId = self::getNextItemId($entityTypeData->parentEntityType, $syncCronState->parentIdLast);
      if ($syncOptions->parentId == 0) {
        $entityTypeNext = self::nextEntityType($syncOptions->entityType);
        $syncCronState = new OsmLocalitiesSyncCronState([
          'entityType' => $entityTypeNext,
        ]);
        $state->set($stateKey, $syncCronState);

        \Drupal::logger('osm_localities')->info('Cron @mode for @entityType completely finished for all parents, switched to next entity type @entityTypeNext .', [
          '@entityType' => $syncOptions->entityType,
          '@entityTypeNext' => $entityTypeNext,
        ]);

        return;
      }
    }

    try {
      $result = OsmLocalities::osmEntityTypeSyncChunk($syncOptions);
      $syncCronState->errorsCount = 0;
    }
    catch (\Exception $e) {
      $syncCronState->errorsCount++;
    }

    if ($result->processed == $syncOptions->limit) {
      // Sync is not finished, saving last id to continue.
      $syncCronState->idLast = $result->idLast;
    }
    elseif ($entityTypeData->parentEntityType && $syncOptions->parentId > 0) {
      $syncCronState->parentIdLast = $syncOptions->parentId;
    }
    else {
      // Fully finished syncing of current entity type, saving timestamps and switching to next.
      OsmLocalities::setEntityTypeSyncTimestamp($syncCronState->entityType, $syncCronState->mode, $syncCronState->timestampStart);

      $syncCronState = new OsmLocalitiesSyncCronState([
        'entityType' => self::nextEntityType($syncOptions->entityType),
      ]);
    }

    \Drupal::logger('osm_localities')->info('Cron @mode for @entityType finished with @processed changes (last id @idLast) in @time sec.', [
      '@mode' => $syncOptions->mode,
      '@entityType' => $syncOptions->entityType,
      '@time' => round(\Drupal::time()->getCurrentMicroTime() - $microtimeStart, 2),
      '@processed' => $result->processed,
      '@idLast' => $result->idLast,
    ]);
    // $syncCronState['stat'] = [
    //   'mode' => $syncOptions->mode,
    //   'entityType' => $syncOptions->entityType,
    //   'time' => round(\Drupal::time()->getCurrentMicroTime() - $microtimeStart, 2),
    //   'processed' => $result->processed,
    //   'idLast' => $result->idLast,
    // ];
    $syncCronState->timestamp = \Drupal::time()->getCurrentTime();

    $state->set($stateKey, $syncCronState);
    $state->set('osm_localities.sync_cron.stat', $result);
  }

  /**
   * Processor for one sync operation.
   *
   * @return int|false|null
   *   Returns id of next item, false if we reached end, null if there is no items in list
   */
  private static function getNextItemId(string $entityType, int $previousId = NULL) {
    $query = \Drupal::entityQuery($entityType)
      ->sort('id')->range(0, 1);
    if ($previousId) {
      $query->condition('id', $previousId, '>');
    }
    $result = $query->execute();
    if (count($result)) {
      return current($result);
    }
    elseif ($previousId) {
      return FALSE;
    }
    else {
      return NULL;
    }
  }

  /**
   * Returns next entity type to sync.
   */
  private static function nextEntityType(string $entityType) {
    $currentEntityTypeDelta = array_search($entityType, self::ENTITY_TYPES);
    $nextEntityType = isset(self::ENTITY_TYPES[$currentEntityTypeDelta + 1])
      ? self::ENTITY_TYPES[$currentEntityTypeDelta + 1]
      : self::ENTITY_TYPES[0];

    return $nextEntityType;
  }

}
