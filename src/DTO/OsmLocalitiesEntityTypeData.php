<?php

namespace Drupal\osm_localities\DTO;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class OsmLocalitiesEntityTypeData.
 *
 * Describes OsmLocalities entity type data.
 */
class OsmLocalitiesEntityTypeData extends DataTransferObject {
  /**
   * Count of processed elements.
   *
   * @var string|null
   */
  public ?string  $parentField;

  /**
   * Count of processed elements.
   *
   * @var string|null
   */
  public ?string $parentFieldLabel;

  /**
   * Count of processed elements.
   *
   * @var string|null
   */

  public ?string $parentEntityType;
  /**
   * Count of processed elements.
   *
   * @var string|null
   */
  public ?string $childEntityType;

  /**
   * Count of processed elements.
   *
   * @var bool
   */
  public bool $emptyChildrenUseParent = FALSE;

  /**
   * Count of processed elements.
   *
   * @var bool
   */
  public bool $selectiveSync = FALSE;

}
