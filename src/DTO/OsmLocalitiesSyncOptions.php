<?php

namespace Drupal\osm_localities\DTO;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class OsmLocalitiesSyncOptions.
 *
 * Describes DTO argument for function osmEntityTypeSyncChunk().
 */
class OsmLocalitiesSyncOptions extends DataTransferObject {
  /**
   * Entity type to sync.
   *
   * @var string
   */
  public string $entityType;

  /**
   * Sync mode: 'update'|'resync'.
   *
   * @var string
   */
  public string $mode;
  // @todo Validate to only 'update'|'resync'!

  /**
   * Id of the parent element.
   *
   * @var int|null
   */
  public ?int $parentId;

  /**
   * Id of starting element to sync.
   *
   * @var int
   */
  public int $fromId = 0;

  /**
   * Limit max amount of elements to sync.
   *
   * @var int
   */
  public int $limit = 100;

  /**
   * Entities filter in Overpass format.
   *
   * @var string
   */
  public string $filter = '';

}
