<?php

namespace Drupal\osm_localities\DTO;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class OsmLocalitiesSyncResult.
 *
 * Describes return DTO of function osmEntityTypeSyncChunk().
 */
class OsmLocalitiesSyncResult extends DataTransferObject {
  /**
   * Count of processed elements.
   *
   * @var int
   */
  public int $processed = 0;

  /**
   * Count of created elements.
   *
   * @var int
   */

  public int $created = 0;

  /**
   * Count of updated elements.
   *
   * @var int
   */
  public int $updated = 0;

  /**
   * Count of deleted elements.
   *
   * @var int
   */
  public int $deleted = 0;

  /**
   * Id of last fetched element.
   *
   * @var int|null
   */
  public ?int $idLast;

  /**
   * Duration of operation in seconds.
   *
   * @var float|null
   */
  public ?float $duration;

  /**
   * Returns count of all changed elements.
   */
  public function changed() {
    return $this->created + $this->updated + $this->deleted;
  }

}
