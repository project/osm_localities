<?php

namespace Drupal\osm_localities\DTO;

use Spatie\DataTransferObject\DataTransferObject;

/**
 * Class OsmLocalitiesCronState.
 *
 * Describes DTO for function Drupal\osm_localities\OsmSyncCron::processIteration().
 */
class OsmLocalitiesSyncCronState extends DataTransferObject {
  /**
   * Entity type to sync.
   *
   * @var string|null
   */
  public ?string $entityType;

  /**
   * Sync mode.
   *
   * @var string|null
   */
  public ?string $mode;

  /**
   * Id of the previous parent element.
   *
   * @var int|null
   */

  public ?int $parentIdLast;

  /**
   * Count of created elements.
   *
   * @var int|null
   */

  public ?int $idLast;

  /**
   * Last operation timestamp.
   *
   * @var int|null
   */

  public ?int $timestamp;
  /**
   * Starting timestamp for current entity type.
   *
   * @var int|null
   */

  public ?int $timestampStart;

  /**
   * Count errors of current chunk execution attempts.
   *
   * @var int
   */
  public int $errorsCount = 0;

}
