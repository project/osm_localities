<?php

namespace Drupal\osm_localities;

use pcrov\JsonReader\JsonReader;

/**
 * Overpass reply file reader as stream.
 */
class OverpassReplyReader {

  private $reader;
  private $readerNext;

  /**
   * The constructor.
   */
  public function __construct(string $filePath) {
    $this->reader = new JsonReader();
    $this->reader->open($filePath);
    if (!$this->reader->read("elements")) {
      throw new \Exception('Elements object is not found in JSON');
    };
    $this->readerNext = NULL;
  }

  /**
   * Reads one element from JSON file and swithes pointer to next element.
   */
  public function getElement() {
    if ($this->readerNext === NULL) {
      $this->reader->read();
    }
    elseif ($this->readerNext === FALSE) {
      return NULL;
    }
    $value = $this->reader->value();
    // We are switching to next element in advance to free up caches from memory.
    $this->readerNext = $this->reader->next();
    if ($this->reader->depth() < 2) {
      $this->readerNext = NULL;
    }
    Utils::forceMemClean();
    return $value;

  }

  /**
   * Closes the parser.
   */
  public function close() {
    $this->reader->close();
  }

}
