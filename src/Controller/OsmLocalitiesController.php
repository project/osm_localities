<?php

namespace Drupal\osm_localities\Controller;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\osm_localities\OsmLocalities;

/**
 * Returns responses for dt1 routes.
 */
class OsmLocalitiesController extends ControllerBase {

  /**
   * Toggle sync mode for entity.
   */
  public function toggleSync($entityType, $entityId) {
    $entity = \Drupal::entityTypeManager()->getStorage($entityType)->load($entityId);
    $entity->is_sync_enabled = $entity->is_sync_enabled ? !$entity->is_sync_enabled->value : TRUE;
    $entity->save();

    return $this->redirect("entity.{$entityType}.collection");
  }

  /**
   * Builds the response.
   */
  public function syncBatch($entityType) {
    $mode = \Drupal::request()->query->get('mode', 'update');
    $parentId = \Drupal::request()->query->get('parent', NULL);
    $destination = \Drupal::request()->query->get('destination')
      ?? Url::fromRoute("entity.{$entityType}.collection");

    $entityTypeStorage = \Drupal::entityTypeManager()->getStorage($entityType);
    $entityTypeLabel = $entityTypeStorage->getEntityType()->getLabel();

    $batchBuilder = new BatchBuilder();

    if (\Drupal::request()->query->get('target', NULL) == 'children') {
      $entityTypeData = OsmLocalities::getEntityTypeData($entityType);
      $entityType = $entityTypeData->childEntityType;
    }

    if ($mode == 'resync') {
      $batchBuilder->setTitle($this->t('Resyncing @entity entities', [
        '@entity' => $entityTypeLabel,
      ]));
    }
    elseif ($mode == 'update') {
      $batchBuilder->setTitle($this->t('Updating @entity entities', [
        '@entity' => $entityTypeLabel,
      ]));
    }

    $batchBuilder
      ->setInitMessage($this->t("Waiting a first reply from Overpass API to count remote elements..."))
      ->setProgressMessage($this->t('Completed @current of @total groups of data.'))
      ->setErrorMessage($this->t('An error has occurred.'));

    $batchBuilder->setFile(
    \Drupal::service('extension.path.resolver')->getPath('module', 'osm_localities') . '/src/OsmSyncBatch.php'
    );
    $batchBuilder->setFinishCallback([
      'Drupal\osm_localities\OsmSyncBatch',
      'finished',
    ]);

    if ($parentId) {
      $parentIds = [$parentId];
    }
    else {
      $entityTypeData = OsmLocalities::getEntityTypeData($entityType);
      if ($entityTypeData->parentEntityType ?? NULL) {
        $parentEntityTypeData = OsmLocalities::getEntityTypeData($entityTypeData->parentEntityType);
        $parentIdsQuery = \Drupal::entityQuery($entityTypeData->parentEntityType);
        if ($parentEntityTypeData->selectiveSync ?? NULL) {
          $parentIdsQuery->condition('is_sync_enabled', 1);
        }
        $parentIds = $parentIdsQuery->execute();
        if ($parentIds && $mode == 'resync') {
          // Deleting exist entities with missing parent.
          $entitiesToDeleteIdsQuery = \Drupal::entityQuery($entityType);
          $entitiesToDeleteIdsQuery->condition(
            $entitiesToDeleteIdsQuery->orConditionGroup()
              ->condition($entityTypeData->parentField, $parentIds, 'NOT IN')
              ->condition($entityTypeData->parentField, NULL, 'IS NULL')
          );
          if ($entitiesToDeleteIds = $entitiesToDeleteIdsQuery->execute()) {
            $entitiesToDelete = $entityTypeStorage->loadMultiple($entitiesToDeleteIds);
            foreach ($entitiesToDelete as $entityToDelete) {
              $entityToDelete->delete();
            }

          }
        }
      }
      else {
        $parentIds = [NULL];
      }
    }

    foreach ($parentIds as $parentId) {
      $batchBuilder->addOperation(
        ['Drupal\osm_localities\OsmSyncBatch', 'process'],
        [
          $entityType,
          $mode,
          $parentId,
        ]
      );
    }

    batch_set($batchBuilder->toArray());

    return batch_process($destination);
  }

}
